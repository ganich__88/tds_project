// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TPS/Character/TPSHealthComponent.h"
#include "TPSCharacterHealthComponent.generated.h"

/**
*
*/
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStaminaChange, float, Stamina);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class TPS_API UTPSCharacterHealthComponent : public UTPSHealthComponent
{
	GENERATED_BODY()
public:

	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Health")
		FOnShieldChange OnShieldChange;
	UPROPERTY(BlueprintAssignable, EditAnyWhere, BlueprintReadWrite, Category = "Health")
		FOnStaminaChange OnStaminaChange;

	FTimerHandle TimerHandle_CollDownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

	FTimerHandle StaminaHandle;

protected:
	virtual void BeginPlay() override;

	float Shield = 100.0f;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated)
	//bool bIsRolls = false;

	UPROPERTY(Replicated)
	float Stamina = 100.0f;

	void RegenerationStamina();

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerLowerStamina(float Value);
	bool ServerLowerStamina_Validate(float Value);
	void ServerLowerStamina_Implementation(float Value);

	UFUNCTION(Server, Reliable, WithValidation)
	void ServerControlSprintingTimer(bool IsSprinting);
	bool ServerControlSprintingTimer_Validate(bool IsSprinting);
	void ServerControlSprintingTimer_Implementation(bool IsSprinting);

public:
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverRate = 0.1f;

	void ChangeHealthValue_OnServer(float ChangeValue) override;

	float GetCurrentShield();
	void ChangeShieldValue(float ChangeValue);
	void CoolDownShieldEnd();
	void RecoveryShield();

	void LowerStamina(float Value);

	UFUNCTION(BlueprintCallable)
	float GetShieldValue();

	UFUNCTION(BlueprintCallable)
	float GetStaminaValue();

	void ControlSprintingTimer(bool IsSprinting);

	UFUNCTION(NetMulticast, Reliable)
	void ShieldChangeEvent_Multicast(float newShield, float Damage);

};
