// Fill out your copyright notice in the Description page of Project Settings.


#include "TPS/Character/TPSCharacterHealthComponent.h"
#include "Net/UnrealNetwork.h"

void UTPSCharacterHealthComponent::BeginPlay()
{
	Super::BeginPlay();
	SetIsReplicated(true);

	GetWorld()->GetTimerManager().SetTimer(StaminaHandle, this, &UTPSCharacterHealthComponent::RegenerationStamina, 1.0f, true);
}

void UTPSCharacterHealthComponent::ChangeHealthValue_OnServer(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);

		if (Shield < 0.0f)
		{

			//FX
			UE_LOG(LogTemp, Warning, TEXT("UTPSCharacterHealthComponent::ChangeHealthValue - Shield < 0"));
		}
	}

	else
	{
		Super::ChangeHealthValue_OnServer(ChangeValue);
	}

}

float UTPSCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UTPSCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	// ���� ������� �������� ������ �� � ���� ��������� ��� if(!bIsRoll) ���� �� ���������
	if (!bIsRoll)
	{
		Shield += ChangeValue;

		//OnShieldChange.Broadcast(Shield, ChangeValue);	// ������� 16 ����
		ShieldChangeEvent_Multicast(Shield, ChangeValue);

		if (Shield > 100.0f)
		{
			Shield = 100.0f;
		}
		else
		{
			if (Shield < 0.0f)
			{
				Shield = 0.0f;
			}
		}

		if (GetWorld())
		{
			GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UTPSCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
}

void UTPSCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UTPSCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UTPSCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoverValue;

	if (Health > 0)
	{
		if (tmp > 100.0f)
		{
			Shield = 100.0f;
			if (GetWorld())
			{
				GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
			}
		}
		else
		{
			Shield = tmp;
		}
	}
	else
	{
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}

	//OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
	ShieldChangeEvent_Multicast(Shield, ShieldRecoverValue);

}

void UTPSCharacterHealthComponent::RegenerationStamina()
{	
	if (GetOwnerRole() == ROLE_Authority)
	{
		if (Stamina >= 100)
		{
			Stamina = 100.0f;
		}
		else
		{
			++Stamina;
			OnStaminaChange.Broadcast(Stamina);
		}
	}
}

bool UTPSCharacterHealthComponent::ServerLowerStamina_Validate(float Value)
{
	return true;
}

void UTPSCharacterHealthComponent::ServerLowerStamina_Implementation(float Value)
{
	if (GetOwnerRole() == ROLE_Authority)
	{
		LowerStamina(Value);
	}
}

void UTPSCharacterHealthComponent::LowerStamina(float Value)
{
	if (GetOwnerRole() < ROLE_Authority)
	{
		ServerLowerStamina(Value);
	}
	else if (GetOwnerRole() == ROLE_Authority)
	{
		if (Stamina - Value < 0.0f)
		{
			Stamina = 0.0f;
		}
		else
		{
				Stamina -= Value;
				OnStaminaChange.Broadcast(Stamina);
		}
	}
}

float UTPSCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}

float UTPSCharacterHealthComponent::GetStaminaValue()
{
	return Stamina;
}

void UTPSCharacterHealthComponent::ControlSprintingTimer(bool IsSprinting)
{
	if (GetOwnerRole() < ROLE_Authority)
	{
		ServerControlSprintingTimer(IsSprinting);
		return;
	}
	else if (GetOwnerRole() == ROLE_Authority)
	{
		if (IsSprinting)
		{
			GetWorld()->GetTimerManager().PauseTimer(StaminaHandle);
		}
		else
		{
			GetWorld()->GetTimerManager().UnPauseTimer(StaminaHandle);
		}
	}
}

bool UTPSCharacterHealthComponent::ServerControlSprintingTimer_Validate(bool IsSprinting)
{
	return true;
}

void UTPSCharacterHealthComponent::ServerControlSprintingTimer_Implementation(bool IsSprinting)
{
	if (GetOwnerRole() == ROLE_Authority)
	{
		ControlSprintingTimer(IsSprinting);
	}
}

void UTPSCharacterHealthComponent::ShieldChangeEvent_Multicast_Implementation(float newShield, float Damage)
{
	OnShieldChange.Broadcast(newShield, Damage);
}

void UTPSCharacterHealthComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UTPSCharacterHealthComponent, Stamina);
}